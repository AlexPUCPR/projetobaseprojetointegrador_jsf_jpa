package br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.mb;

import static br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.mb.PesquisarArtistaMB.ARTISTA_SELECIONADA;
import static br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.mb.PesquisarArtistaMB.FILTRO_PESQUISA;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import br.pucpr.bsi.projetoIntegrador.nomeProjeto.bc.ArtistaBC;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.exception.BSIException;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.model.Artista;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.mb.utils.ViewUtil;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.view.messages.MessagesUtils;

/**
 * @author Everson Mauda
 * @version 1.0.0
 */

@ManagedBean
@ViewScoped
public class ManterArtistaMB implements Serializable{
	
	public enum Acoes {
		EDITAR, EXCLUIR, INCLUIR, VISUALIZAR;
	}
	
	/////////////////////////////////////
	// Atributos
	/////////////////////////////////////
	
	private static final long serialVersionUID = 1L;
	
	//Utilizado para logs via Log4J
	private static Logger log = Logger.getLogger(ManterArtistaMB.class);
	
	private Artista artista = new Artista();
	private Artista filtroPesquisa;
	
	private Acoes acao = Acoes.INCLUIR;
	
	/////////////////////////////////////
	// Construtores
	/////////////////////////////////////
	
	public ManterArtistaMB() {
	}
	
	@PostConstruct
	private void init(){
		acao = (Acoes) ViewUtil.getParameter(Acoes.class);
		if(acao != null && !Acoes.INCLUIR.equals(acao)){
			artista = (Artista) ViewUtil.getParameter(ARTISTA_SELECIONADA);
			if(artista == null){
				throw new BSIException("ER0052");
			}
			log.debug("Valor do Artista:" + artista);
			filtroPesquisa = (Artista) ViewUtil.getParameter(FILTRO_PESQUISA);
			log.debug("Valor do Filtro:" + filtroPesquisa);
		} else  {
			acao = Acoes.INCLUIR;
		}
	}	
	
	/////////////////////////////////////
	// Actions
	/////////////////////////////////////
	
	
	//Action de Salvar Usuario
	public String salvar() {
		if(isAcaoIncluir()){
			ArtistaBC.getInstance().insert(this.artista);
		} else if(isAcaoEditar()){
			ArtistaBC.getInstance().update(this.artista);
		} else if(isAcaoExcluir()){
			ArtistaBC.getInstance().delete(this.artista);
		}
		MessagesUtils.addInfo("sucesso", "IN0000");
		return voltar();
	}
	
	public String voltar(){
		ViewUtil.setRequestParameter(FILTRO_PESQUISA, filtroPesquisa);
		return "pesquisarArtista";
	}
	
	//Action para reset do cadastro
	public void limpar() {
		artista = new Artista();
	}
	
	/////////////////////////////////////
	// Metodos Utilitarios
	/////////////////////////////////////
	
	public boolean isAcaoEditar(){
		return Acoes.EDITAR.equals(acao);
	}
	
	public boolean isAcaoExcluir(){
		return Acoes.EXCLUIR.equals(acao);
	}	
	
	public boolean isAcaoIncluir(){
		return Acoes.INCLUIR.equals(acao);
	}

	public boolean isAcaoVisualizar(){
		return Acoes.VISUALIZAR.equals(acao);
	}
	
	/////////////////////////////////////
	// Getters and Setters
	/////////////////////////////////////
	
	public Artista getArtista() {
		return artista;
	}
	
	public void setAviao(Artista aviao) {
		this.artista = aviao;
	}
	
	public void setAcao(Acoes acao) {
		this.acao = acao;
	}
	
	public String getTitle(){
		switch(acao){
			case EDITAR:
				return MessagesUtils.getLabel("editarArtista");
			case EXCLUIR:
				return MessagesUtils.getLabel("excluirArtista");
			case INCLUIR:
				return MessagesUtils.getLabel("incluirArtista");
			case VISUALIZAR:
				return MessagesUtils.getLabel("visualizarArtista");
		}
		return null;
	}
}